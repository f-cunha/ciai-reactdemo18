export interface Company {
    name: string;
    address: string;
    city: string;
    country: string;
    logo: string
    employees:User[]
}

export interface User {
    name: string;
    age: number;
    email: string;
    telephone: string;
}