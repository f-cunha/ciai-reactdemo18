import axios from 'axios';
import * as React from 'react';

import './App.css';
import Clock from './Clock';

import {CompanyDetails, CompanyList} from "./Companies";
import {Company} from "./Interfaces";
import logo from './logo.svg';
import {UserDetails, UsersList} from "./UserDetails";

interface IApp {
    selectedCompany:number | null;
    selectedUser: number | null;
    companies:Company[];
}

class App extends React.Component<{},IApp> {

    constructor(props:{}) {
        super(props);
        this.state = {selectedCompany: null, selectedUser: null, companies:[]};
    }

    public componentWillMount() {
        axios
            .get('./companieswemployees.json')
            .then( (json:any) => {
                this.setState({companies:json.data});
            });
    }

    public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <div>
            <Clock/>
            <CompanyList list={this.state.companies}
                         select={this.selectCompany}
                         pagesize={10}/>

            { this.state.selectedCompany === null ? '' :
                <CompanyDetails company={this.state.companies[this.state.selectedCompany]}/> }

            { this.state.selectedCompany === null ||
              this.state.companies[this.state.selectedCompany].employees.length === 0 ? '' :
                <UsersList list={this.state.companies[this.state.selectedCompany].employees}
                            select={this.selectUser}/> }

            { this.state.selectedCompany === null ||
              this.state.selectedUser === null ? '' :
                <UserDetails user={this.state.companies[this.state.selectedCompany].employees[this.state.selectedUser]}/> }
        </div>
      </div>
    );
  }

  private selectCompany = (x:number) => {this.setState({selectedCompany:x, selectedUser:null})};

  private selectUser = (x:number) => {this.setState({selectedUser:x})};

}

export default App;
