import * as React from "react";
import {PagedList} from "./PagedList";

interface ISearchableList<T> {
    title:string;
    list:T[];
    select:(x:number) => void;
    show:(x:T) => string;
    page?:number;
    pagesize?:number;
    predicate: (x:T,s:string) => boolean
}

export class FilteredList<T> extends React.Component<ISearchableList<T>,{search:string}> {
    constructor(props:ISearchableList<T>) {
        super(props);
        this.state = {search:''};
    }

    public render() {
        const {list, predicate, ...otherprops} = this.props;

        return (
          <div>
              <label htmlFor='search'>Search:</label>
              <input id='search' value={this.state.search} onChange={this.setValue}/>

              <PagedList {...otherprops}
                         list={list.filter((s:T)=>predicate(s,this.state.search))}/>
          </div>
        );
    }

    private setValue = ({target:{value}}:any) => this.setState({search:value});
}