import * as React from "react";

class Clock extends React.Component<{},{time:Date}> {
    private timer:any = null;

    constructor(props:{}) {
        super(props);
        this.state = {time:new Date()};
    }

    public componentDidMount() {
        this.timer = setInterval(()=>this.setState({time:new Date()}), 1000)
    }

    public componentWillUnmount() {
        clearInterval(this.timer);
    }

    public render() {
        return <div>{this.state.time.toLocaleTimeString()}</div>;
    }
}

export default Clock;