import {Company} from "./Interfaces";

export interface ICompanyList {
    list: Company[];
    select: (x: number) => void;
    page?: number;
    pagesize?: number;
}