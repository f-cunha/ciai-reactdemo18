import * as React from "react";

export interface IPagedList<T> {
    title:string;
    list:T[];
    select:(x:number) => void;
    show:(x:T) => string;
    page?:number;
    pagesize?:number;
}

export class PagedList<T> extends React.Component<IPagedList<T>, {page?:number, pagesize?:number}> {
    private max: number = 0;

    constructor(props: IPagedList<T>) {
        super(props);

        this.state = {
            page:props.page,
            pagesize:props.pagesize,
        };

        this.max = Math.ceil(props.list.length/(props.pagesize || 1))-1;
    }

    public render() {
        const {title, list, select, show} = this.props;
        const {page = 0, pagesize = list.length} = this.state;
        this.max = Math.ceil(list.length/pagesize)-1;

        return (<div>
                    <h1>{title}</h1>
                    <ul>
                        {list
                            .slice(page*pagesize, page*pagesize + pagesize)
                            .map(
                                (c, i) => (<li key={i}
                                               onClick={() => select(i)}>
                                        {show(c)}
                                    </li>
                                ))
                        }
                    </ul>
                    <button onClick={this.prevPage}>previous</button>
                    <span>{page+1}</span>
                    <button onClick={this.nextPage}>next</button>
                </div>);
    }

    private prevPage = () => {
        this.setState((st) => ({page:((st.page===undefined)?0:Math.max(0,st.page-1))}));
    };
    private nextPage = () => {
        this.setState((st) => ({page:((st.page===undefined)?0:Math.min(this.max,st.page+1))}));
    };
}