import * as React from "react";
import {FilteredList} from "./FilteredList";
import {ICompanyList} from "./ICompanyList";
import {Company} from "./Interfaces";

export class CompanyList extends React.Component<ICompanyList,{}> {
    constructor(props:ICompanyList) {
        super(props);
    }

    public render() {
        return <FilteredList<Company> {...this.props}
                                      title={"Companies"}
                                      show={this.show}
                                      predicate={this.predicate}/>
    }

    private show = (c:Company) => `${c.name} (${c.country})`;

    private predicate = (c:Company,s:string) => (c.name+c.country).indexOf(s) !== -1;
}

export const CompanyDetails = (props: { company: Company }) => (
    <div>
        <img src={props.company.logo}/>
        <h2>{props.company.name}</h2>
        <p>{props.company.address}({props.company.city}, {props.company.country})</p>
    </div>);